package com.example.applicationmedia

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity

class VideoActivity : AppCompatActivity() {

    private lateinit var videoView: VideoView
    private lateinit var videoSeekBar: SeekBar
    private lateinit var totalTime: TextView
    private lateinit var audioButton: Button

    private lateinit var handler: Handler
    private var isTrackingTouch = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        videoView = findViewById(R.id.videoView)
        videoSeekBar = findViewById(R.id.videoSeekBar)
        totalTime = findViewById(R.id.totalTime)
        audioButton = findViewById(R.id.audioButton)

        val videoUri = intent.data
        videoView.setVideoURI(videoUri)

        handler = Handler()

        audioButton.setOnClickListener {
            if (videoView.isPlaying) {
                videoView.pause()
            } else {
                videoView.start()
            }
        }

        videoSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    videoView.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                isTrackingTouch = true
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                isTrackingTouch = false
            }
        })

        videoView.setOnPreparedListener { mediaPlayer ->
            videoSeekBar.max = mediaPlayer.duration
            setTime(mediaPlayer.currentPosition, mediaPlayer.duration)
        }

        videoView.start()

        updateSeekBar()
    }

    @SuppressLint("SetTextI18n")
    private fun updateSeekBar() {
        val currentPosition = videoView.currentPosition
        val duration = videoView.duration

        setTime(currentPosition, duration)

        if (!isTrackingTouch) {
            videoSeekBar.progress = currentPosition
        }

        handler.postDelayed({ updateSeekBar() }, 1000)
    }

    @SuppressLint("SetTextI18n")
    private fun setTime(currentPosition: Int, duration: Int) {
        val currentTime = formatTime(currentPosition)
        val durationTime = formatTime(duration)

        totalTime.text = "$currentTime/$durationTime"
    }

    private fun formatTime(milliseconds: Int): String {
        val seconds = milliseconds / 1000
        val minutes = seconds / 60
        val remainingSeconds = seconds % 60
        return String.format("%02d:%02d", minutes, remainingSeconds)
    }
}
