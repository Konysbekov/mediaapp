package com.example.applicationmedia

import android.media.MediaPlayer
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class AudioActivity : AppCompatActivity() {

    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var audioPath: TextView
    private lateinit var audioButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)

        audioPath = findViewById(R.id.audioPath)
        audioButton = findViewById(R.id.audioButton)

        val uri = intent.data
        audioPath.text = uri.toString()
        mediaPlayer = MediaPlayer().apply {
            if (uri != null) {
                setDataSource(applicationContext, uri)
            }
            prepare()
        }

        mediaPlayer.setOnPreparedListener {
            mediaPlayer.start()
        }

        audioButton.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
            } else {
                mediaPlayer.start()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release()
    }
}
