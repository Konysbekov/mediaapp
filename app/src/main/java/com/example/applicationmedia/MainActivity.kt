package com.example.applicationmedia

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var audioButton: Button
    private lateinit var videoButton: Button

    private val audioActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val uri = result.data?.data
                val intent = Intent(this, AudioActivity::class.java)
                intent.data = uri
                startActivity(intent)
            }
        }

    private val videoActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val uri = result.data?.data
                val intent = Intent(this, VideoActivity::class.java)
                intent.data = uri
                startActivity(intent)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        audioButton = findViewById(R.id.audioButton)
        videoButton = findViewById(R.id.videoButton)

        audioButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "audio/*"
            audioActivityResult.launch(Intent.createChooser(intent, "Select Audio"))
        }

        videoButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "video/*"
            videoActivityResult.launch(Intent.createChooser(intent, "Select Video"))
        }
    }
}
